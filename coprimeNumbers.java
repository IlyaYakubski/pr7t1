public class coprimeNumbers {
    public static boolean coprime(int first, int second, int third) {
        int scd = 1;
        for (int i = 1; i <=Math.abs(first) && i <= Math.abs(second) && i <= Math.abs(third); i++) {
            if (first % i == 0 && second % i == 0 && third % i == 0) {
                scd = i;
            }
        }
        return scd == 1;
    }

    public static void main(String[] args) {
        System.out.println(coprime(45, 1962, -1));
    }
}

